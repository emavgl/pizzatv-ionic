import { ManualInteractionException } from "./ManualInteractionException";

export class RecaptchaException extends ManualInteractionException {
    public url: string;
    constructor(message: string, url: string) {
        super(message, url);
    }
}