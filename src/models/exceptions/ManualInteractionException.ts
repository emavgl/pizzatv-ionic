export class ManualInteractionException extends Error {
    public url: string;

    constructor(message: string, url: string) {
        super(message); // 'Error' breaks prototype chain here
        this.url = url;
        Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
    }
}