import { ManualInteractionException } from "./ManualInteractionException";

export class CloudflareException extends ManualInteractionException {
    public url: string;
    constructor(message: string, url: string) {
        super(message, url);
    }
}