export class Episode {
    id: number;
    name: string;
    overview: string;
    season_number: number;
    episode_number: number;
    backdrop_path: string;
    backdrop_path_thumbnail: string;
}