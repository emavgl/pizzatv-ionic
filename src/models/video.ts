export interface Video {
    channel: string;
    url: string;
    server: string;
    is_sub: string;
    parsedUrl: string;
    quality: string;
}