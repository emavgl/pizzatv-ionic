import { Episode } from './episode';

export class Season {
    id: number;
    episode_count: number;
    poster_path: string;
    season_number: number;
    episodes: Episode[];
}