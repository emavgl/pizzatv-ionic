import { Show } from './show';
import { Season } from './season';

export class TVShow extends Show {
    original_name: string;
    number_of_episodes: number;
    number_of_seasons: number;
    seasons: Season[];
}