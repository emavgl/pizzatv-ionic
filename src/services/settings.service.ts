import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class SettingService {
    constructor(private storage: Storage){       
    }

    setDefault(key: string) : any {
        let defaultValue = null;
        console.log('default value for key', key);
        if (key == 'fastSearch'){ defaultValue = false; }
        if (key == 'useCache'){ defaultValue = false; }
        if (key == 'showReccomanded'){ defaultValue = false; }
        if (key == 'simplifiedInterface'){ defaultValue = false; }
        if (key == 'cols'){ defaultValue = -1; }
        if (key == 'baseurl'){ defaultValue = "https://pizzatv.evgl.ovh"; }
        this.setSetting(key, defaultValue);
        return defaultValue;     
    }

    async getSetting(key: string): Promise<any> {
        let value: any = await this.storage.get(key + 'settings');
        if (value == null || value == undefined){
            value = this.setDefault(key);
        }
        return value;
    }

    setSetting(key: string, value: any): void {
        this.storage.set(key + 'settings', value);
    }
}