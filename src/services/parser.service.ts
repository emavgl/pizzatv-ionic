import { Injectable } from '@angular/core';
import { Http } from '@angular/http';


import { Mixdrop } from '../modules/urlresolver/servers/mixdrop'
import { Wstream } from '../modules/urlresolver/servers/wstream'
import { Speedvideo } from '../modules/urlresolver/servers/speedvideo'
import { Akvideo } from '../modules/urlresolver/servers/akvideo'
import { Vcrypt } from '../modules/urlresolver/servers/vcrypt';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ParserService {
    constructor(private http: Http){        
    }

    async extractUrl(url: string): Promise<string> {

        // TODO: add vcrypt
        if (url.includes('vcrypt')) {
            console.log("vcrypt extraction", url);
            let vcrypt = new Vcrypt(this.http);
            url = await vcrypt.getDestinationUrl(url);
            console.log("new url from vcrypt");
        }

        console.log("Extracting", url);
        if (url.includes('speedvideo')) {
            let speedvideoService = new Speedvideo(this.http);
            return speedvideoService.extractUrl(url);            
        } else if (url.includes('wstream')) {
            let wstreamService = new Wstream(this.http);
            return wstreamService.extractUrl(url);            
        }
        else if (url.includes('mixdrop')) {
            let mixdropService = new Mixdrop(this.http);
            return mixdropService.extractUrl(url);            
        } 
        else if (url.includes('akvideo')) {
            let akvideoService = new Akvideo(this.http);
            return akvideoService.extractUrl(url);            
        } else {
            return Promise.resolve(url);   
        }
    }
}