import { Injectable } from '@angular/core';
import { Episode } from '../models/episode';
import { Storage } from '@ionic/storage';

@Injectable()
export class HistoryService {
    constructor(private storage: Storage){       
    }

    async getLastEpisode(showId: string, target: string): Promise<Episode> {
        let key = showId + target;
        return this.storage.get(key);
    }

    saveEpisode(showId: string, target: string, episode: Episode): void {
        let key = showId + target;
        this.storage.set(key, episode);
    }

    saveLastShow(showId: string, target: string): void {
        this.storage.set('last' + target, showId);
    }

    getLastShow(target: string): Promise<string> {
        return this.storage.get('last' + target);
    }
}