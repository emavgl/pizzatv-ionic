import { Component, Input, Output, EventEmitter} from '@angular/core';
import { Show } from '../../models/show';

import { ToastService } from '../../services/toast.service';
import { SettingService } from '../../services/settings.service';

@Component({
  selector: 'show-grid',
  templateUrl: 'showgrid.html',
  providers: [ToastService, SettingService]
})
export class ShowgridComponent {

  cols: number;
  _shows: Show[];
  _simplifiedInterface: boolean;
  _infiniteScroll: boolean;

  constructor(public toastService: ToastService, private settingService: SettingService) {
  }

  async ngOnInit() {
    this.cols = await this.settingService.getSetting('cols');
  }

  // Handle inputs
  @Input()
  set shows(shows: Show[]) {
    this._shows = shows;
  }
  get shows() { return this._shows; }

  @Input()
  set simplifiedInterface(simplifiedInterface: boolean) {
    this._simplifiedInterface = simplifiedInterface;
  }
  get simplifiedInterface() { return this._simplifiedInterface; }

  @Input()
  set infiniteScroll(infiniteScroll: boolean) {
    this._infiniteScroll = infiniteScroll;
  }
  get infiniteScroll() { return this._infiniteScroll; }

  // Handle events
  @Output() showTap = new EventEmitter<Show>();
  onTapEventEmitter(item: Show) {
      this.showTap.emit(item);
  }

  @Output() infiniteEvent = new EventEmitter<Event>();
  onIonInfiniteEventEmitter(event: Event) {
    this.infiniteEvent.emit(event);
  }

}
