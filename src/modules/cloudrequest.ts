import { CloudflareException } from "../models/exceptions/CloudflareException";
import { Http } from '@angular/http';
import { RecaptchaException } from "../models/exceptions/RecaptchaException";
import { ManualInteractionException } from "../models/exceptions/ManualInteractionException";

export class CloudRequest {

    private http: Http;
    
    constructor(http: Http) {
        this.http = http;
    }

    async get(url: string) {
        try {
            let response = await this.http.get(url).toPromise();
            this.checkRecaptha(response.text(), url);
            return response;
        } catch (error) {
            this.checkCloudflareException(error.text(), url);
            this.checkRecaptha(error.text(), url);
            throw error;
        }
    }

    async post(url: string, formData: FormData) {
        try {
            let response = await this.http.post(url, formData).toPromise();
            this.checkRecaptha(response.text(), url);
            return response;
        } catch (error) {
            this.checkCloudflareException(error.text(), url);
            this.checkRecaptha(error.text(), url);
            throw error;
        }
    }

    private checkCloudflareException(htmlDoc: string, url: string) {
        if (htmlDoc.includes("allow_5_secs")) {
            // Cloudflare challenge to pass
            throw new CloudflareException("Cloudflare prompt a 5 seconds challenge", url);
        }
    } 


    private checkRecaptha(htmlDoc: string, url: string) {
        if (htmlDoc.includes("recaptcha/api")) {
            throw new RecaptchaException("The page contain a captcha to bypass", url);
        }
    } 

}