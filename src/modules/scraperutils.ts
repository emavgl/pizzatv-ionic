export class ScraperUtils {

    static findMultipleMatch = function(data, reg){
        reg = RegExp(reg, 'g')
        var results = [];
        var match;
        while ((match = reg.exec(data)) !== null) {
            results.push(match);
        }
        return results;
    }

}