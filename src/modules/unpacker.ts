export class Unpacker {

    static findUnpackerScript = function(code) {
        const regex = /eval([\s\S]*?)<\/script>/gm;
        var matches, output = [];
        while (matches = regex.exec(code)) {
            output.push("eval" + matches[1].trim());
        }
        output = output.filter((x) => x.includes(`function(p,a,c,k,e,d)`))
        return output;
    }

    static unpack = function (str) {
        var params = Unpacker.filterargs(str);
        var payload = params[0], symtab = params[1], radix = params[2], count = params[3];
        if (count != symtab.length) {
            throw new Error("Malformed p.a.c.k.e.r. symtab. (" + count + " != " + symtab.length + ")");
        }
        var unbase = Unpacker.unbaser(radix);
        var lookup = (word) => symtab[unbase(word)] || word;
        let source = payload.replace(/\b\w+\b/g, lookup);
        return source;
    }

    static filterargs = function(str) {
        /* [\s\S] insteadof . because javascript has no dotall modifier */
        var juicers = [ 
            /}\('([\s\S]*)', *(\d+), *(\d+), *'([\s\S]*)'\.split\('\|'\), *(\d+), *([\s\S]*)\)\)/,
            /}\('([\s\S]*)', *(\d+), *(\d+), *'([\s\S]*)'\.split\('\|'\)/
        ];
        for (var c = 0; c < juicers.length; ++c) {
            var m, juicer = juicers[c];
            if (m = juicer.exec(str)) {
                return [m[1], m[4].split('|'), parseInt(m[2]), parseInt(m[3])];
            }
        }
        throw new Error("Could not make sense of p.a.c.k.e.r data (unexpected code structure)");
    }

    static alphabet = {
        62: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
        95: '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~'
    }

    static unbaser = function(base)
    {
        if (2 <= base && base <= 36) return (str) => parseInt(str, base);
        var dictionary = {};
        var alphabet = Unpacker.alphabet[base];
        if (!alphabet) throw new Error("Unsupported encoding");
        for (var c = 0; c < alphabet.length; ++alphabet) {
            dictionary[alphabet[c]] = c;
        }
        return (str) => str.split("").reverse().reduce((cipher, ind) => Math.pow(base, ind) * dictionary[cipher]);
    }
    

}