import { Http } from '@angular/http';
import { CloudRequest } from "../../cloudrequest";

export class Vcrypt {

    private cloudRequest: CloudRequest;

    constructor(private http: Http){       
        this.cloudRequest = new CloudRequest(http);
    }

    async getDestinationUrl(vcryptUrl) {
        let redirectedUrl = await this.getRedirectedUrl(vcryptUrl, 0);
        if (redirectedUrl) {
            if (redirectedUrl.includes("4snip")) {
                return this.bypass4snip(redirectedUrl);
            } else {
                return redirectedUrl;
            }
        } else {
            return null;
        }
    }

    async bypass4snip(snipUrl) {
        let pathParameters = snipUrl.split("/")
        let postUrl = "https://4snip.pw/outlink/" + pathParameters[pathParameters.length - 1];

        let formData = new FormData();
        formData.append("url", pathParameters[pathParameters.length - 1]);

        console.log("Doing fucking request");

        try {
            let postResponse = await this.http.post(postUrl, formData).toPromise();
            return postResponse.url;
        } catch (error) {
            if (error.headers && error.headers.location) {
                return error.headers.location;
            }
            if (error.url) return error.url;
            throw error;
        }
    }   

    async getRedirectedUrl(toRedirect, counter) {
        let res = await this.cloudRequest.get(toRedirect);
        if (counter > 2) return res.url;

        let body = res.text();
        if (body.includes("refresh")) {
            const regex = /URL=(https[^">]*)/gm;
            let m;
            while ((m = regex.exec(body)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                
                // The result can be accessed through the `m`-variable.
                let newUrl = m[1];
                return this.getRedirectedUrl(newUrl, counter+1);
            }
        }

        return res.url;
    }
}