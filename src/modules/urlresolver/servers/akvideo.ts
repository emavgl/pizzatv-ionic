import { Http } from '@angular/http';
import { IServer } from '../interfaces/IServer'
import { Unpacker } from "../../unpacker";
import { ScraperUtils } from "../../scraperutils";
import { CloudRequest } from "../../cloudrequest";


export class Akvideo implements IServer {
    private cloudRequest: CloudRequest;

    constructor(private http: Http){       
        this.cloudRequest = new CloudRequest(http);
    }

    async extractUrl(url: string) : Promise<string> {
        console.log('akvideo url', url)

        let response = await this.cloudRequest.get(url);
        let content = response.text();
        let unpackScripts = Unpacker.findUnpackerScript(content);

        // iterate to find first mp4 link
        for (const unpackScript of unpackScripts) {
            try {
                let clearScript = Unpacker.unpack(unpackScript);
                if (clearScript.includes("Clappr")) {
                    let videoUrls = ScraperUtils.findMultipleMatch(clearScript, /https:\/\/([^\s,<]+\.mp4)/g);
                    videoUrls = videoUrls.filter(v => !v.includes('akvideo'));
                    if (videoUrls) {
                        return videoUrls[0][0];
                    }
                }
            } catch (error) {
                continue;
            }
        }

        return null;
    }
}