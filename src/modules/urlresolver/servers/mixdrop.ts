import { Http } from '@angular/http';
import { IServer } from '../interfaces/IServer'
import { Unpacker } from "../../unpacker";
import { ScraperUtils } from "../../scraperutils";


export class Mixdrop implements IServer {
    constructor(private http: Http){        
    }

    async extractUrl(url: string) : Promise<string>{
        console.log('mixdrop url', url)

        let response =  await this.http.get(url).toPromise();
        let content: string = response.text();
        let unpackScripts = Unpacker.findUnpackerScript(content);

        // iterate to find first mp4 link
        for (const unpackScript of unpackScripts) {
            try {
                let clearScript = Unpacker.unpack(unpackScript);
                let videoUrls1 = ScraperUtils.findMultipleMatch(clearScript, /src="\/\/(.*\.mp4[^"]+)/gm);
                let videoUrls2 = ScraperUtils.findMultipleMatch(clearScript, /vsr="\/\/(.*\.mp4[^"]+)/gm);
                let videoUrls = videoUrls1.length != 0 ? videoUrls1 : videoUrls2;
                if (videoUrls) {
                    return 'https://' + videoUrls[0][1];
                }
            } catch (error) {
                console.log(error);
                continue;
            }
        }
    
        return null;
    }
}