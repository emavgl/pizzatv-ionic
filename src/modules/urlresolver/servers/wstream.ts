import { Http } from '@angular/http';
import { IServer } from '../interfaces/IServer'
import { Unpacker } from "../../unpacker";
import { ScraperUtils } from "../../scraperutils";
import { CloudRequest } from "../../cloudrequest";
import { ManualInteractionException } from '../../../models/exceptions/ManualInteractionException';

export class Wstream implements IServer {

    private cloudRequest: CloudRequest;

    constructor(private http: Http){       
        this.cloudRequest = new CloudRequest(http);
    }

    async transformUrl(url: string, attempt: number) {

        if (attempt > 2) {
            console.log("Exceeded number of attemps");
            return url;
        }
      
        let response = await this.cloudRequest.get(url);
        let htmlDoc = response.text();
        
        // get all inputs
        let formData = this.getInputFormValues(htmlDoc);

        if (formData != null) {
            try {
                let res = await this.cloudRequest.post(url, formData);
                let content: any = res.text();
                if (content) {
                    let location = res.headers.get('location');
                    if (content.includes("Continue") || (location && location.includes("file_code"))) {
                        if (location) {
                            return await this.transformUrl(location, attempt+1);
                        } else {
                            return await this.transformUrl(res.url, attempt+1);
                        }
                    }
                }
                return res.url;
            } catch (error) {

                if (error instanceof ManualInteractionException) {
                    throw error; // should be handled somewhere else
                }

                // If we can handled execption ...
                if (error.headers && error.headers.location) {
                    let location = error.headers.location;
                    return await this.transformUrl(location, attempt+1);
                }
                return await this.transformUrl(url, attempt+1);
            }
        }

        return await this.transformUrl(url, attempt+1);
    }

    private getInputFormValues(htmlDoc: string) : FormData | null {
        let regex = /input type=.hidden.*name=.([a-z]+).*value=.([a-z0-9]+)./gm;
            
        let m;
        let formData: FormData = new FormData();
        let counter = 0;
        while ((m = regex.exec(htmlDoc)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            
            // The result can be accessed through the `m`-variable.
            formData.append(m[1], m[2]);
            counter += 1;
        }

        if (counter == 0) {
            // retry with another regex
            regex = /input type=.hidden.*id=.([a-z]+).*value=.([a-z0-9]+)./gm;
            while ((m = regex.exec(htmlDoc)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                
                // The result can be accessed through the `m`-variable.
                formData.append(m[1], m[2]);
                counter += 1;
            }
        }
        
        if (counter == 0) return null;

        return formData;
    }

    async extractUrl(url: string) : Promise<string>{
        console.log('wstream url', url)

        let transformedUrl = await this.transformUrl(url, 0);
        console.log("transformedUrl", transformedUrl);

        let response = await this.cloudRequest.get(transformedUrl);
        let content: string = response.text();

        let unpackScripts = Unpacker.findUnpackerScript(content);

        // iterate to find first mp4 link
        for (const unpackScript of unpackScripts) {

            try {
                let clearScript = Unpacker.unpack(unpackScript);
                let videoUrls = ScraperUtils.findMultipleMatch(clearScript, /"(https:\/\/[^"]*v.mp4)"/g);
                if (videoUrls) {
                    let finalUrl = videoUrls[0][1];
                    let fakeUrls = ScraperUtils.findMultipleMatch(finalUrl, /\/3.\//g); 
                    if (fakeUrls.length == 0) return finalUrl;
                }
            } catch (error) {
                if (error.message && error.message.includes("Malformed")) {
                    console.log("Cannot unpack: skipped");
                }
                continue;
            }

        }

        let videoUrls = ScraperUtils.findMultipleMatch(content, /"(https:\/\/[^"]*v.mp4)"/g);
        if (videoUrls && videoUrls.length > 0) return videoUrls[0][1];
    
        return null;
    }
}