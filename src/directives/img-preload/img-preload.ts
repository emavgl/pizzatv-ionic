import { Directive, Input, OnInit } from '@angular/core';

/**
 * Generated class for the ImagePreloader directive.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
 * for more info on Angular Directives.
 */
@Directive({
  selector: '[img-preload]',
  host: {
    '[attr.src]': 'finalImage'  // the attribute img[src] is now linked with finalImage variables
  }
})
export class ImgPreloadDirective implements OnInit {
  @Input('img-preload') targetSource: string;
  downloadingImage : any; // In class holder of remote image
  finalImage: any; //property bound to our host attribute.

  // Set an input so the directive can set a default image.
  @Input() defaultImage : string = 'assets/placeholder.png';

  constructor() {
    // pass
  }

  //ngOnInit is needed to access the @inputs() variables. these aren't available on constructor()
  ngOnInit() {
    // Set final image to the placeholder
    this.finalImage = this.defaultImage;

    // Create a new Image and set the attribute src in this way, our image will be downloaded in background
    this.downloadingImage = new Image();
    this.downloadingImage.src = this.targetSource;

    this.downloadingImage.onload = () => {
      // Image is downloaded; set finalImage
      this.finalImage = this.targetSource;
    }

    this.downloadingImage.onerror = () => {
        // do nothing
        // use placeholder images
    }
  }
}
