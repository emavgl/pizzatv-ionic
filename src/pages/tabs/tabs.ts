import { Component } from '@angular/core';

import { FavoritePage } from '../favorite/favorite';
import { HomePage } from '../home/home';
import { SettingsPage } from '../settings/settings';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = HomePage;
  tab3Root = FavoritePage;
  tab4Root = SettingsPage;

  target_film = 'movie';
  target_tv = 'tv';
  favorite = 'favorite';
  settings = 'settings';
  

  constructor(){}
}
