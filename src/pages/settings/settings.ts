import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { ToastService } from '../../services/toast.service';
import { SettingService } from '../../services/settings.service';


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
  providers: [ToastService, SettingService]
})
export class SettingsPage {

  baseurl: string;
  fastSearch: boolean;
  showReccomanded: boolean;
  simplifiedInterface: boolean;
  useCache: boolean;
  cols: number;
  
  constructor(public navCtrl: NavController, public toastService: ToastService,
              public settingService: SettingService, private alertCtrl: AlertController) {
  }

  // Runs when the page is about to enter and become the active page.  
  ionViewWillEnter(): void {
    this.settingService.getSetting('baseurl').then(baseurl  => this.baseurl = baseurl);
    if (this.baseurl == 'false') this.baseurl  = "";
    this.settingService.getSetting('fastSearch').then(fastSearch  => this.fastSearch = fastSearch);
    this.settingService.getSetting('useCache').then(useCache  => this.useCache = useCache);
    this.settingService.getSetting('simplifiedInterface').then(simplifiedInterface  => this.simplifiedInterface = simplifiedInterface);    
    this.settingService.getSetting('showReccomanded').then(showReccomanded  => this.showReccomanded = showReccomanded);    
    this.settingService.getSetting('cols').then(cols  => this.cols = cols); 
  }

  isURL(url: string): boolean {
    let regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    let isValid: boolean = regex.test(url);
    return (isValid && !url.endsWith('/'))
  }

  showRebootMessage() : void {
    let alert = this.alertCtrl.create({
      title: 'Riavvio necessario',
      subTitle: "Dopo aver salvato, riavviare l'app per applicare i cambiamenti.",
      buttons: ['OK']
    });
    alert.present();
  }

  save(): void {
    if (this.simplifiedInterface) this.showReccomanded = false;
    this.settingService.setSetting('fastSearch', this.fastSearch);
    this.settingService.setSetting('showReccomanded', this.showReccomanded);
    this.settingService.setSetting('useCache', this.useCache);
    this.settingService.setSetting('simplifiedInterface', this.simplifiedInterface);
    this.settingService.setSetting('cols', this.cols);
    if (this.baseurl && this.isURL(this.baseurl)){
      this.toastService.showTextToast('Impostazioni salvate');
      this.settingService.setSetting('baseurl', this.baseurl);    
    } else {
      // alert
      let alert = this.alertCtrl.create({
        title: 'URL non corretta',
        subTitle: 'Devi inserire una URL valida',
        buttons: ['OK']
      });
      alert.present();
    }
  }
}
