### How to start:

```bash
# install ionic and cordova
$ sudo npm install -g ionic cordova

# Copy this repo
$ git clone https://bitbucket.org/emavgl/pizzatv-ionic.git
$ cd pizzatv-ionic

# Remove streaming-media-plugin from package.js (don't ask)
Remove from package.json any row with streaming-media

# Install dependencies
$ npm install

# Reinstall the plugin
$ ionic cordova plugin add cordova-plugin-streaming-media
$ npm install --save @ionic-native/streaming-media

# Be sure to have installed also
- JAVA and set JAVA_HOME
- ANDROID_STUDIO and set ANDROID_HOME
- GRADLE and set GRADLE_HOME
```

You have now completed the installation

```bash
# to run on browser
$ ionic lab

# to run on device
$ ionic cordova run android --device
```

### How to contribute:

1. Fork this repository. 
2. Do something with your forked code.
3. Submit a pull requests.

Good practice: https://gist.github.com/Chaser324/ce0505fbed06b947d962